package main;

import java.util.Arrays;

public class Board {
    char[][] board;
    int size;
    public Board(int size) {
        this.size = size;
        board = new char[size][size];
    }
    public Board copy() {
        Board copyBoard = new Board(size);
        for (int i = 0; i < board.length; i++) {
            copyBoard.board[i] = Arrays.copyOf(board[i], board[i].length);
        }
        return copyBoard;
    }

    public void makeMove(Symbol s, Move m) throws IllegalMoveException {
        if (board[m.x][m.y] != 0) {
            System.out.println(board[m.x][m.y] + " is already marked. Please try again!");
            throw new IllegalMoveException();
        }
        board[m.x][m.y] = s.symbol;
    }
    public char getElement(int i,int j) {
        return board[i][j];
    }
}
