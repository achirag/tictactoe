package main;

import java.util.ArrayList;

public class TicTacToe {
    int size;
    int winSequence;
    Board board;
    ScoreBoard scoreBoard;
    ArrayList<Player> player;
    int moves = 0;

    public TicTacToe(int n, int k) {
        this.size = n;
        this.winSequence = k;
        init();
    }


    public boolean takeTurn() {
        Player player = nextPlayer();
        System.out.println("Turn " + player.getSymbol());

        try {
            Move m = player.getNextMove(this.scoreBoard.copy(), board.copy(), winSequence);
            board.makeMove(player.getSymbol(), m);
            scoreBoard.updateScore(m,player.getSymbol().value);
            generateBoard();
            moves++;
            int checkScore = checkGame();
            if (checkScore != 0) {
                return false;
            }
        } catch (IllegalMoveException e) {
            System.out.println(e);
        } catch (LostMatchException ignored) {
            System.out.println(player.getSymbol() +" has resigned.");
            return false;
        }

        return true;
    }

    private int checkGame() {
        if (scoreBoard.isWon()!=0) {
            return winSequence+1;
        }
        if (moves < size * size) {
            return 0;
        }
        System.out.println("Game is a tie");
        return winSequence + 1;
    }

    public void generateBoard() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                printCell(board.getElement(i,j)); // print each of the cells
                if (j != size - 1) {
                    System.out.print("|");   // print vertical partition
                }
            }
            System.out.println();
            if (i != size - 1) {
                System.out.println("---".repeat(size) + "-".repeat(size - 1)); // print horizontal partition
            }
        }
        System.out.println();
    }

    private void printCell(char content) {
        if (content == 0) {
            System.out.print("   ");
        } else {
            System.out.print(" " + content + " ");
        }
    }

    public void addPlayer(Player p) {
        player.add(p);
    }

    private Player nextPlayer() {
        return player.get(moves % 2);
    }

    private void init() {
        board =new Board(size);
        scoreBoard = new ScoreBoard(size,winSequence);
        player = new ArrayList<>(2);
        generateBoard();
    }
}
