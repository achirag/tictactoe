package main;

import java.util.Scanner;

public class HumanPlayer implements Player{
    Scanner sc;
    Symbol symbol;
    public HumanPlayer(Scanner sc,Symbol symbol){
        this.sc = sc;
        this.symbol = symbol;
    }
    public Symbol getSymbol(){
        return symbol;
    }
    public Move getNextMove(ScoreBoard scoreBoard, Board board, int k) {
        String input = sc.next();
        String[] index = input.split(",");
        int x = Integer.parseInt(index[0]);
        int y = Integer.parseInt(index[1]);
        return new Move(x,y);
    }
}
