package main;

import java.util.ArrayList;

public class MaxScoreBot implements Player{
    Symbol symbol;
    MaxScoreBot opponent;
    public MaxScoreBot(Symbol symbol) {
        this.symbol = symbol;
        this.opponent = new MaxScoreBot();
        this.opponent.symbol = symbol==Symbol.X?Symbol.O:Symbol.X;
    }
    private MaxScoreBot(){

    }

    public Move getNextMove(ScoreBoard scoreBoard, Board board, int k)throws LostMatchException{
        return findBestMove(scoreBoard,board,1);
    }
    public Symbol getSymbol(){
        return symbol;
    }

    private Move findBestMove(ScoreBoard scoreBoard, Board board,int level) throws LostMatchException{
        int initialScore = this.calculateScore(scoreBoard);
        int maxDiff = 0;
        ArrayList<Move> allPossibleMoves = this.legalMoves(board);
        Move res = null;
        for (Move move : allPossibleMoves) {
            ScoreBoard newScore = this.getNewScore(move, scoreBoard);
            int sc = this.calculateScore(newScore);
            if (sc==10000) {
                return move;
            }
            if (maxDiff < Math.abs(initialScore - sc)) {
                if(level>0){
                    Board updatedBoard = board.copy();
                    try {
                        updatedBoard.makeMove(symbol, move);
                        Move opponentBestMove = opponent.findBestMove(newScore, updatedBoard, level - 1);
                        newScore = opponent.getNewScore(opponentBestMove,newScore);
                        updatedBoard.makeMove(opponent.symbol,opponentBestMove);
                        try{
                            this.findBestMove(newScore,updatedBoard,level-1);
                        }catch (LostMatchException lmE) {
                            if(res==null){
                                res = opponentBestMove;
                            }
                            continue; // do not consider in case of LostMatch
                        } catch (Exception ignored){

                        }
                    } catch (Exception ignored) {
                    }
                }
                maxDiff = Math.abs(initialScore - sc);
                res = move;
            }
        }
        if (res==null&&allPossibleMoves.size()!=0){
            throw new LostMatchException();
        }
        return res;
    }

    private ScoreBoard getNewScore(Move m, ScoreBoard scoreBoard) {
        ScoreBoard newScoreBoard = scoreBoard.copy();
        newScoreBoard.updateScore(m,this.symbol.value);
        return newScoreBoard;
    }

    private int calculateScore(ScoreBoard scoreBoard) {
        int score = 0;
        for (int sc : scoreBoard.scoreBoard) {
            if(Math.abs(sc)==scoreBoard.winSequence) {
                if (sc* getSymbol().value>0){
                    return 10000;
                }
            } else {
                score += sc * Math.abs(sc);
            }
            if (Math.abs(sc)==scoreBoard.winSequence-1){
                if (sc* getSymbol().value<0) { //opponent is winning, don't consider this move
                    return 0;//2nd preference to stop
                }
            }
        }
        return score;
    }


    private ArrayList<Move> legalMoves(Board board) {
        ArrayList<Move> result = new ArrayList<>();
        for (int i = 0; i < board.size; i++) {
            for (int j = 0; j < board.size; j++) {
                if (board.getElement(i,j) == 0) {
                    result.add(new Move(i, j));
                }
            }
        }
        return result;
    }
}
