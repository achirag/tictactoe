package main;

import java.util.Arrays;

//n*(n-k+1) rows and same columns, 2*(n-k+1)^2
// i,j
//i*(n-k+1)+max((j-k),0) - i*(n-k+1) + j
// scoreboard[i*(n-k+1)+x]+=val
//j*(n-k+1)+max((i-k),0) - j*(n-k+1) + i
//  //0,3
//1 1
// i=j i+1=j .. i+(n-k)=j  i=j+1 i+1=j+1
// for y -> (0,n-k)
//     for x -> (0,n-k)
//        if (i+x==j+y)
//         scoreboard[y*(n-k)+x]++

// i+j+1=n i+j+2=n i+j+1=n+1 i+j+2=n+1
/* for y -> (0,n-k)
       for x -> (0,n-k)
        if (i+j+1+x == n+y)
          scoreboard[y*(n-k)+x]+=val
 */
public class ScoreBoard {
    int[] scoreBoard;
    int size;
    int winSequence;

    public ScoreBoard(int n,int k){
        this.size = n;
        this.winSequence = k;
        this.scoreBoard = new int[getScoreBoardSize()];
    }
    public ScoreBoard copy() {
        ScoreBoard copy = new ScoreBoard(this.size,this.winSequence);
        copy.initScoreboard(this.scoreBoard);
        return copy;
    }
    public void initScoreboard(int[] scoreBoard) {
        this.scoreBoard = Arrays.copyOf(scoreBoard,scoreBoard.length);
    }
    public void updateScore(Move move,int val) {
        rowScoring(move, val);
        columnScoring(move, val);
        diagonalScoring(move, val);
    }
    public int isWon() {
        for (int i : scoreBoard) {
            if (Math.abs(i) == winSequence) {
                if (i < 0) {
                    System.out.println("O has won");
                } else {
                    System.out.println("X has won");
                }
                return i;
            }
        }
        return 0;
    }
    private void rowScoring(Move move,int val) {
        int rowIndent = move.x * (size - winSequence + 1);
        for (int x = Math.max((move.y - winSequence + 1), 0); x <= move.y; x++) {
            if (x + winSequence <= size) {
                int index = rowIndent + x;
                scoreBoard[index] = scoreBoard[index] + val;
            }
        }
    }

    private void columnScoring(Move move, int val) {
        int rows = size * (size - winSequence + 1);
        int columnIndent = move.y * (size - winSequence + 1);
        for (int y = Math.max((move.x - winSequence + 1), 0); y <= move.x; y++) {
            if (y + winSequence <= size) {
                int index = rows + columnIndent + y;
                scoreBoard[index] = scoreBoard[index] + val;
            }
        }
    }

    private void diagonalScoring(Move move, int val) {
        int rows = size * (size - winSequence + 1);
        int columns = size * (size - winSequence + 1);
        int diagonal = (int) Math.pow(size - winSequence + 1, 2);
        for (int y = 0; y <= size - winSequence; y++) {
            for (int x = 0; x <= size - winSequence; x++) {
                if (move.x + x == move.y + y && move.x < x + winSequence && move.x >= x) {
                    int index = rows + columns + (y * (size - winSequence + 1) + x);
                    scoreBoard[index] = scoreBoard[index] + val;
                }
                if (move.x + move.y + 1 + x == size + y) {
                    int index = rows + columns + diagonal + (y * (size - winSequence + 1) + x);
                    scoreBoard[index] = scoreBoard[index] + val;
                }
            }
        }
    }

    private int getScoreBoardSize() {
        return (int) (2 * size * (size - winSequence + 1) + 2 * (Math.pow(size - winSequence + 1, 2)));
    }
}
