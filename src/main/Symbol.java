package main;

public enum Symbol {
    X(1,'X'),O(-1,'O');
    int value;
    char symbol;
    Symbol(int i,char s) {
        symbol = s;
        value = i;
    }
}
