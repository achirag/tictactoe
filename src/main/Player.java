package main;

public interface Player {
    Move getNextMove(ScoreBoard scoreBoard, Board board, int k) throws LostMatchException;
    Symbol getSymbol();
}
