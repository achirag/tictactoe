package main;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the box: ");
        int size = sc.nextInt();

        System.out.println("Enter the tokens in sequence to win: ");
        int winSequence = sc.nextInt();

        System.out.println("Computer first?");
        boolean computerFirst = sc.nextBoolean();

        TicTacToe game = new TicTacToe(size, winSequence);

        Player bot = new MaxScoreBot(computerFirst ? Symbol.X : Symbol.O);
        Player human = new HumanPlayer(sc, computerFirst ? Symbol.O : Symbol.X);

        if(computerFirst){
            game.addPlayer(bot);
            game.addPlayer(human);
        } else {
            game.addPlayer(human);
            game.addPlayer(bot);
        }
        while (game.takeTurn()){}
    }
}
